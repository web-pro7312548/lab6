import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Buu</h1></body></html>';
    // return this.appService.getHello();
  }

  @Post('world')
  getWorld(): string {
    return '<html><body><h1>World Buu</h1></body></html>';
    // return this.appService.getHello();
  }

  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsicus') celsicus: number,
    @Query('type') type: string,
  ) {
    return {
      celsicus: celsicus,
      type: type,
    };
  }

  @Get('test-params/:celsicus')
  testParams(@Req() req, @Param('celsicus') celsicus: number) {
    return { celsicus };
  }

  @Post('test-body')
  testBody(@Req() req, @Body() Body, @Body('celsicus') celsicus: number) {
    return celsicus;
  }
}
