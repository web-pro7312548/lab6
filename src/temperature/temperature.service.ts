import { Injectable } from '@nestjs/common';

@Injectable()
export class TemperatureService {
  convert(celsicus: number) {
    return {
      celsicus: celsicus,
      fahrenheit: (celsicus * 9.0) / 5 + 32,
    };
  }
}
