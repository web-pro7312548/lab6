import { Controller, Get, Param, Query } from '@nestjs/common';
import { TemperatureService } from './temperature.service';

@Controller('temperature')
export class TemperatureController {
  constructor(private readonly temperatureService: TemperatureService) {}
  @Get('convert')
  convert(@Query('celsicus') celsicus: string) {
    return this.temperatureService.convert(parseFloat(celsicus));
  }

  @Get('convert')
  convertByPost(@Query('celsicus') celsicus: string) {
    return this.temperatureService.convert(parseFloat(celsicus));
  }

  @Get('convert/:celsicus')
  convertByParam(@Param('celsicus') celsicus: string) {
    return this.temperatureService.convert(parseFloat(celsicus));
  }
}
